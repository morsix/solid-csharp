﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID_CSharp._3_LSP
{
    public abstract class Fruit
    {
        public abstract string GetColor();
    }
}
