﻿namespace SOLID_CSharp._4_ISP
{
    public interface IOnlineOrder
    {
        void ProcessCreditCard();
    }
}