﻿namespace SOLID_CSharp._4_ISP
{
    public class OnlineOrder : IOrder, IOnlineOrder
    {
        public void ProcessCreditCard()
        {
            // process through credit card
        }

        public void Purchase()
        {
            // Do purchase
        }
    }
}