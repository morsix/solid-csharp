﻿namespace SOLID_CSharp._4_ISP
{
    public interface IOrder
    {
        void Purchase();
    }
}