﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID_CSharp._2_OCP
{
    public class CarController
    {
        private List<ICar> cars;

        public CarController()
        {
            cars = new List<ICar>();
            cars.Add(new Audi());
            cars.Add(new Mercedes());
        }

        public string GetCarMileage(string name)
        {
            return cars.First(car => car.Name == name).GetMileage();
        }
    }
}
