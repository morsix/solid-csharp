﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID_CSharp._2_OCP
{
    public class Mercedes : ICar
    {
        public string Name { get; set; }

        public string GetMileage()
        {
            return "20M";
        }
    }
}
