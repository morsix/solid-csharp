﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID_CSharp._2_OCP
{
    public class MileageCalculator
    {
        private IEnumerable<ICar> _cars;

        public MileageCalculator(IEnumerable<ICar> cars)
        {
            _cars = cars;
        }

        public void CalculateMileage()
        {
            CarController controller = new CarController();

            foreach (var car in _cars)
            {
                Console.WriteLine("Mileage of the car {0} is {1}", car.Name, controller.GetCarMileage(car.Name));
            }
        }
    }
}
