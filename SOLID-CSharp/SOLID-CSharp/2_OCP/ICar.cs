﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID_CSharp._2_OCP
{
    public interface ICar
    {
        string Name { get; set; }

        string GetMileage();
    }
}
