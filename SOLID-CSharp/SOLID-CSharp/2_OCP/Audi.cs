﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID_CSharp._2_OCP
{
    public class Audi : ICar
    {
        public string Name { get; set; }

        public string GetMileage()
        {
            return "10M";
        }
    }
}
