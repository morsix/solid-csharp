﻿using System;

namespace SOLID_CSharp._5_DIP
{
    public class Truck : IVehicle
    {
        public void Accelerate()
        {
            Console.WriteLine("Truck accelerates...");
        }

        public void Brake()
        {
            Console.WriteLine("Truck stopped.");
        }
    }
}