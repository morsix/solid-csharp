﻿namespace SOLID_CSharp._5_DIP
{
    public interface IVehicle
    {
        void Accelerate();

        void Brake();
    }
}