﻿using System;

namespace SOLID_CSharp._5_DIP
{
    public class Car : IVehicle
    {
        public void Accelerate()
        {
            Console.WriteLine("Car accelerates...");
        }

        public void Brake()
        {
            Console.WriteLine("Car stopped.");
        }
    }
}