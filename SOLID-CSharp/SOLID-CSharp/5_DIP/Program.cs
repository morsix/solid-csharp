﻿using System;

namespace SOLID_CSharp._5_DIP
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            //IVehicle vehicle = new Car();

            IVehicle vehicle = new Truck();
            var vehicleController = new VehicleController(vehicle);
            vehicle.Accelerate();
            vehicle.Brake();

            Console.Read();
        }
    }
}