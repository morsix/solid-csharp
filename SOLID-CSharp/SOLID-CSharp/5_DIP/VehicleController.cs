﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID_CSharp._5_DIP
{
    public class VehicleController
    {
        private IVehicle _vehicle;

        public VehicleController(IVehicle vehicle)
        {
            _vehicle = vehicle;
        }

        public void Accelerate()
        {
            _vehicle.Accelerate();
        }

        public void Brake()
        {
            _vehicle.Brake();
        }
    }
}
