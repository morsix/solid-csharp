﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID_CSharp._1_SRP
{
    public class ServiceStation
    {
        private IGateUtility _gateUtility;

        public ServiceStation(IGateUtility gateUtility)
        {
            _gateUtility = gateUtility;
        }

        public void OpenForService()
        {
            _gateUtility.OpenGate();
        }

        public void DoService()
        {
            //Check if service station is opened and then
            //complete the vehicle service
        }

        public void CloseForDay()
        {
            _gateUtility.CloseGate();
        }
    }
}
