﻿namespace SOLID_CSharp._1_SRP
{
    public interface IGateUtility
    {
        void OpenGate();

        void CloseGate();
    }
}